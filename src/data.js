import logo from "./assets/images/logo-og.png";

export const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: logo,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100,
    countPercentStudyingStudents() {
      return this.studyingStudents / this.totalStudents * 100;
    }
}
