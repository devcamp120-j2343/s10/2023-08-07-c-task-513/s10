import devcampReact from "./assets/images/devcampreact.png";
import { gDevcampReact } from "./data";

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>

      <img src={gDevcampReact.image} width={500} alt="Background"/>

      <p>Tỷ lệ sinh viên đang theo học: {gDevcampReact.countPercentStudyingStudents()} %</p>

      <p>Tỷ lệ sinh viên đăng ký học {gDevcampReact.countPercentStudyingStudents() > 15 ? "nhiều" : "ít" }</p>

      <ul>
        {
          gDevcampReact.benefits.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
      </ul>

      <img src={devcampReact} alt="Devcamp React" width={1000}/>
    </div>
  );
}

export default App;
